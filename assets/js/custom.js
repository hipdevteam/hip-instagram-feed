;(function ($) {

	$('#hip_instagram_feed').owlCarousel({
		center: true,
		items: 5,
		loop:true,
		margin:30,
		nav: true, 
		dots: false,
		autoWidth:!0,
		navText: ['<span class="hip_instagram_feed_arrow left-arrow"></span>', '<span class="hip_instagram_feed_arrow right-arrow"></span>'],
		responsiveClass:true,
		responsive:{
			0:{
				items:3,
				autoWidth:true,
			},
			600:{
				items:3,
				autoWidth:true,
			},
			1025:{
				items:4,
				autoWidth:true,
				
			}
		}
		
	});

})(jQuery);
